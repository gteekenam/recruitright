lazy val akkaHttpVersion  = "10.1.10"
lazy val akkaVersion      = "2.5.25"
lazy val scalaTestVersion = "3.0.5"
lazy val logbackVersion   = "1.0.6"
lazy val kafkaavroVersion = "3.0.1"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "com.recruitright",
      scalaVersion    := "2.12.8"
    )),
    name := "RecruitRight",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion  % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion      % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion      % Test,
      "org.scalatest"     %% "scalatest"            % scalaTestVersion % Test,
      "io.kamon"          %% "kamon-logback"        % logbackVersion,
      "io.confluent" % "kafka-avro-serializer"      % kafkaavroVersion
    )
  )
